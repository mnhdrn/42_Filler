# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: clrichar <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/02/20 15:08:30 by clrichar          #+#    #+#              #
#    Updated: 2018/07/17 18:41:45 by clrichar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			:=			clrichar.filler

#==============================================================================#
#------------------------------------------------------------------------------#
#                               DIRECTORIES                                    #

SRC_DIR			:=			./srcs
INC_DIR			:=			./includes
OBJ_DIR			:=			./obj
LIB_DIR			:=			./libft
LIBFT			:=			./libft/libft.a

#==============================================================================#
#------------------------------------------------------------------------------#
#                                  FILES                                       #

SRC				:=			filler.c			\
							m_init.c			\
							m_map.c				\
							m_piece.c			\
							m_algo.c			\
							m_fill.c			\
							m_process.c			\
							utils.c				\

OBJ				:=			$(addprefix $(OBJ_DIR)/,$(SRC:.c=.o))
NB				:=			$(words $(SRC))
INDEX			:=			0

#==============================================================================#
#------------------------------------------------------------------------------#
#                            COMPILER & FLAGS                                  #

CC				:=			gcc
CFLAGS			:=			-Wall -Wextra -Werror
OFLAGS			:=			-pipe
CFLAGS			+=			$(OFLAGS)
CLIB			:=			-L$(LIB_DIR) -lft

#==============================================================================#
#------------------------------------------------------------------------------#
#                                LIBRARY                                       #

L_FT			:=			$(LIB_DIR)


#==============================================================================#
#------------------------------------------------------------------------------#
#                                 RULES                                        #

all:					$(NAME)


$(NAME):				$(LIBFT) $(OBJ_DIR) $(OBJ)
	@$(CC) $(OFLAGS) $(OBJ) $(CLIB) -o $(NAME)
	@cp $(NAME) ./resources/players/$(NAME)
	@printf '\033[33m[ 100%% ] %s\n\033[0m' "Compilation of $(NAME) is done ---"


$(OBJ_DIR)/%.o:			$(SRC_DIR)/%.c
	@$(eval DONE=$(shell echo $$(($(INDEX)*20/$(NB)))))
	@$(eval PERCENT=$(shell echo $$(($(INDEX)*100/$(NB)))))
	@$(eval TO_DO=$(shell echo "$@"))
	@$(CC) $(CFLAGS) -I$(INC_DIR) -o $@ -c $<
	@printf "[ %d%% ] %s :: %s        \r" $(PERCENT) $(NAME) $@
	@$(eval INDEX=$(shell echo $$(($(INDEX)+1))))


$(OBJ_DIR):
	@mkdir -p $(OBJ_DIR)


$(LIBFT):
	@make -C libft/ --no-print-directory


clean:
	@make -C $(L_FT) clean --no-print-directory  
	@rm -f $(OBJ)
	@rm -rf $(OBJ_DIR)
	@printf '\033[33m[ KILL ] %s\n\033[0m' "Clean of $(NAME) is done ---"


fclean: 				clean
	@rm -rf $(NAME)
	@rm -rf ./resources/players/$(NAME)
	@make -C $(L_FT) fclean --no-print-directory
	@printf '\033[33m[ KILL ] %s\n\033[0m' "Fclean of $(NAME) is done ---"


re:	
	@$(MAKE) fclean
	@$(MAKE) 


.PHONY: all clean fclean re build cbuild

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 18:39:34 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/17 18:13:10 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# include "libft.h"
# include <stdlib.h>
# include <unistd.h>
# include <stdbool.h>

typedef struct		s_ply
{
	char			**map;
	char			**piece;
	int				**weight;
	char			ply_char;
	char			foe_char;
	int				cy;
	int				cx;
	int				py;
	int				px;
	int				my;
	int				mx;
	int				big;
}					t_ply;

extern bool			init_map(t_ply *data);
extern bool			init_game(t_ply *data);
extern bool			init_piece(t_ply *data);
extern bool			init_weight(t_ply *data);
extern bool			m_map(t_ply *data, char *line);
extern bool			m_piece(t_ply *data);
extern void			m_process(t_ply *data);
extern bool			m_fill(t_ply *data);
extern void			m_algo(t_ply *data);
extern bool			ft_overflow(char *line);
extern void			ft_int_del(int ***as, int y);
extern void			ft_clean(t_ply *data);

#endif

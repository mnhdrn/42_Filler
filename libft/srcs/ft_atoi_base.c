/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:34 by clrichar          #+#    #+#             */
/*   Updated: 2018/06/29 17:35:51 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int			ft_isbase(char c, size_t base)
{
	c = (char)ft_toupper(c);
	if (base <= 10)
		return ((c >= '0' && c <= '9'));
	return ((c >= '0' && c <= '9') || (c >= 'A' && c <= ('A' + ((int)base
						- 10))));
}

static void			get_value(int *i, char c, size_t base)
{
	c = (char)ft_toupper(c);
	if ((c - 'A' >= 0))
		*i = (*i * (int)base) + (c - 'A' + 10);
	else
		*i = (*i * (int)base) + (c - '0');
}

int					ft_atoi_base(const char *str, size_t base)
{
	int				i;
	int				neg;

	i = 0;
	neg = 0;
	while (*str && ((*str >= 7 && *str <= 13) || *str == ' '))
		str++;
	if (*str == '+')
		str++;
	else if (*str == '-')
	{
		neg = 1;
		str++;
	}
	while (ft_isbase(*str, base))
	{
		get_value(&i, *str, base);
		str++;
	}
	return ((neg == 1) ? -i : i);
}

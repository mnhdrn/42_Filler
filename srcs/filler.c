/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/17 18:02:29 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/17 18:04:16 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static inline void		print_result(t_ply *data)
{
	char				*s1;
	char				*s2;

	s1 = ft_itoa(data->cy);
	s2 = ft_itoa(data->cx);
	(s1) ? ft_putstr(s1) : 0;
	ft_putchar(' ');
	(s2) ? ft_putstr(s2) : 0;
	ft_putchar('\n');
	(s1) ? ft_strdel(&s1) : 0;
	(s2) ? ft_strdel(&s2) : 0;
}

static bool				play(t_ply *data, char *line)
{
	if (!m_map(data, line))
		return (false);
	if (!m_piece(data))
		return (false);
	if (!init_weight(data))
		return (false);
	m_process(data);
	m_algo(data);
	if (data->cy == 0 && data->cx == 0)
		m_fill(data);
	return (true);
}

int						main(void)
{
	char				*line;
	t_ply				data;

	line = NULL;
	ft_bzero(&data, sizeof(data));
	if (!init_game(&data))
		return (-1);
	while (get_next_line(0, &line) > 0)
	{
		if (!play(&data, line))
			break ;
		ft_clean(&data);
		(line) ? ft_strdel(&line) : 0;
		print_result(&data);
	}
	(line) ? ft_strdel(&line) : 0;
	ft_clean(&data);
	return (EXIT_SUCCESS);
}

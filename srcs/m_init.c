/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_init.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 10:10:20 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/17 17:58:36 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

bool			init_game(t_ply *data)
{
	int			rd;
	int			ret;
	char		*line;

	ret = 0;
	line = NULL;
	if ((rd = get_next_line(0, &line)) < 0)
		return (false);
	if (ret == 0 && (!line || ft_strlen(line) < 11
				|| !ft_strnstr(line, "$$$ ", 4)))
		ret = 1;
	if (ret == 0 && ft_strnequ(line, "$$$ exec p1", 11))
	{
		data->ply_char = 'O';
		data->foe_char = 'X';
	}
	else if (ret == 0 && ft_strnequ(line, "$$$ exec p2", 11))
	{
		data->ply_char = 'X';
		data->foe_char = 'O';
	}
	else
		ret = 1;
	(line) ? ft_strdel(&line) : 0;
	return ((ret == 0) ? true : false);
}

bool			init_map(t_ply *data)
{
	int					i;

	i = 0;
	if (!(data->map = (char **)malloc(sizeof(char *)
					* ((size_t)data->my + 1))))
		return (false);
	while (i < data->my)
	{
		if (!(data->map[i] = ft_strnew((size_t)data->mx)))
			return (false);
		i++;
	}
	data->map[i] = NULL;
	return (true);
}

bool			init_piece(t_ply *data)
{
	int					i;

	i = 0;
	if (!(data->piece = (char **)malloc(sizeof(char *)
					* ((size_t)data->py + 1))))
		return (false);
	while (i < data->py)
	{
		if (!(data->piece[i] = ft_strnew((size_t)data->px)))
			return (false);
		i++;
	}
	data->piece[i] = NULL;
	return (true);
}

bool			init_weight(t_ply *data)
{
	int					i;

	i = 0;
	if (!(data->weight = (int **)malloc(sizeof(int *)
					* ((size_t)data->my + 1))))
		return (false);
	while (i < data->my)
	{
		if (!(data->weight[i] = ft_int_tabmake((size_t)data->mx + 1)))
			return (false);
		i++;
	}
	return (true);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_map.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 10:10:23 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/16 17:05:44 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static bool		map_size(t_ply *data, char *line)
{
	int			ret;
	char		**tab;

	tab = NULL;
	ret = 0;
	if (ret == 0 && (!line || ft_strlen(line) < 12
				|| !ft_strnequ(line, "Plateau ", 7)))
		ret = 1;
	if (ret == 0 && (!(tab = ft_strsplit(line, ' '))))
		ret = 1;
	if (ret == 0 && (!ft_overflow(tab[1]) || !ft_overflow(tab[2])))
		ret = 1;
	data->my = (ret == 0) ? ft_atoi(tab[1]) : 0;
	data->mx = (ret == 0) ? ft_atoi(tab[2]) : 0;
	(tab) ? ft_str_tabdel(&tab) : 0;
	return ((ret == 0) ? true : false);
}

static bool		map_row(void)
{
	int			rd;
	int			ret;
	char		*line;

	line = NULL;
	ret = 0;
	if ((rd = get_next_line(0, &line)) < 0)
		ret = 1;
	if (ret == 0 && (!line || ft_strlen(line) < 5 || !ft_isdigit(line[5])))
		ret = 1;
	if (ret == 0 && !ft_strnequ("    0", line, 5))
		ret = 1;
	(line) ? ft_strdel(&line) : 0;
	return ((ret == 0) ? true : false);
}

static bool		map_stock(t_ply *data, char **line, int n)
{
	int			i;
	int			ret;

	i = 0;
	ret = 0;
	if (!(*line) || ft_strlen(*line) < 6 || !ft_isdigit((*line)[0])
			|| !ft_isdigit((*line)[1]) || !ft_isdigit((*line)[2]))
		ret = 1;
	if (ret == 0 && ft_strlen(&(*line)[4]) != (size_t)data->mx)
		ret = 1;
	while ((*line)[i])
	{
		if (i > 4 && ft_strnchr(".oOxX", (*line)[i]) == 0)
		{
			ret = 1;
			break ;
		}
		i++;
	}
	(ret == 0) ? ft_strncpy(data->map[n], &(*line)[4], (size_t)data->mx) : 0;
	(*line) ? ft_strdel(&(*line)) : 0;
	return ((ret == 0) ? true : false);
}

static bool		map_get(t_ply *data, int rd, int n)
{
	char		*line;

	line = NULL;
	if (!data->map)
	{
		if (!init_map(data))
			return (false);
	}
	if (!map_row())
		return (false);
	while ((rd = get_next_line(0, &line)) > 0 && n < (data->my - 1))
	{
		if (!map_stock(data, &line, n))
			return (false);
		n++;
	}
	(line) ? ft_strdel(&line) : 0;
	return (true);
}

bool			m_map(t_ply *data, char *line)
{
	if (!map_size(data, line))
		return (false);
	if (!map_get(data, 0, 0))
		return (false);
	return (true);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_process.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 14:39:31 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/17 18:11:17 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static void			ft_cut_h(t_ply *data)
{
	int				i;
	int				middle;

	i = 0;
	middle = data->my / 2;
	if (middle < 1)
		return ;
	while (i < data->mx)
	{
		if (data->map[middle][i] == '.')
			data->weight[middle][i] = 1;
		i++;
	}
}

static void			ft_set_base(t_ply *data, int y, int x)
{
	if (((y - 1) >= 0) && data->map[y - 1][x] == '.')
		data->weight[y - 1][x] = 2;
	if (((y + 1) < data->my) && data->map[y + 1][x] == '.')
		data->weight[y + 1][x] = 2;
	if (((x - 1) >= 0) && data->map[y][x - 1] == '.')
		data->weight[y][x - 1] = 2;
	if (((x + 1) < data->mx) && data->map[y][x + 1] == '.')
		data->weight[y][x + 1] = 2;
}

static void			prepare(t_ply *data)
{
	int				i;
	int				j;

	i = 0;
	while (i < data->my)
	{
		j = 0;
		while (j < data->mx)
		{
			if (data->map[i][j] == data->foe_char
					|| data->map[i][j] == (data->foe_char + 32))
				ft_set_base(data, i, j);
			j++;
		}
		i++;
	}
	return ;
}

void				m_process(t_ply *data)
{
	prepare(data);
	ft_cut_h(data);
	data->big = 0;
}

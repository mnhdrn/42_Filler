/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/17 18:01:54 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/17 18:01:55 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void					ft_int_del(int ***as, int y)
{
	int					i;

	i = 0;
	if (!as || y <= 0)
		return ;
	while (i < y)
	{
		ft_int_tabdel(&(*as)[i]);
		i++;
	}
	free((*as));
	(*as) = NULL;
}

void					ft_clean(t_ply *data)
{
	if (data->map)
		ft_str_tabdel(&data->map);
	if (data->piece)
		ft_str_tabdel(&data->piece);
	if (data->weight)
		ft_int_del(&data->weight, data->my);
}

bool					ft_overflow(char *line)
{
	long long			ret;

	if (!line || ft_strlen(line) < 1)
		return (false);
	ret = ft_atoll(line);
	if (ret > 0 && ret < 1000)
		return (true);
	return (false);
}
